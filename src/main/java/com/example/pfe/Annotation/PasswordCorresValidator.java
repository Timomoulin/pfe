package com.example.pfe.Annotation;
import com.example.pfe.DTO.UtilisateurDTO;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;


public class PasswordCorresValidator implements ConstraintValidator<PasswordCorres, Object> {

    @Override
    public void initialize(PasswordCorres constraintAnnotation) {
    }

    @Override
    public boolean isValid(Object obj, ConstraintValidatorContext context) {
        if (obj instanceof UtilisateurDTO) {
            UtilisateurDTO utilisateur = (UtilisateurDTO) obj;
            return utilisateur.getMdp1() != null && utilisateur.getMdp1().equals(utilisateur.getMdp2());
        }
        return false;
    }
}
