package com.example.pfe.DAO;
import com.example.pfe.Model.Admin;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AdminDAO extends JpaRepository<Admin, Long> {

}
