package com.example.pfe.DAO;

import com.example.pfe.Model.Commentaire;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CommentaireDAO extends JpaRepository<Commentaire, Long> {
    Page<Commentaire> findByNoteContainingIgnoreCase(String key, Pageable pageable);


}
