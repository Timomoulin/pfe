package com.example.pfe.DAO;

import com.example.pfe.Model.Tuteur;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TuteurDAO extends JpaRepository<Tuteur, Long> {
    Page<Tuteur> findByNomContainsIgnoreCase(String key, Pageable pageable);


}
