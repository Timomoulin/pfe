package com.example.pfe.DAO;

import com.example.pfe.Model.Equipe;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EquipeDAO extends JpaRepository<Equipe,Long> {
    Page<Equipe> findByNomContainsIgnoreCase(String key, Pageable pageable);

    List<Equipe> getByNom(String nom);








}
