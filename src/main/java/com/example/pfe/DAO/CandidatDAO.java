package com.example.pfe.DAO;

import com.example.pfe.Model.Candidat;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CandidatDAO extends JpaRepository<Candidat, Long> {
    Page<Candidat> findByNomContainingIgnoreCase(String key, Pageable page);


}
