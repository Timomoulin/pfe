package com.example.pfe.Controller.Admin;

import com.example.pfe.DTO.UtilisateurDTO;
import com.example.pfe.Model.Candidat;
import jakarta.validation.Valid;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

public class adminController {

    @GetMapping("/admin/candidat/createCandidat")
    public String createCandidat(@ModelAttribute UtilisateurDTO utilisateurDTO) {

        return "admin/candidat/createCandidat";
    }

    @PostMapping("/admin/candidat/createCandidat")
    public String storeCandidat(Model model, @ModelAttribute @Valid UtilisateurDTO utilisateurDTO, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("errorMessage", "Un problème est survenu lros du traitement ");
            model.addAttribute("candidats", candidatService.getAll());
            return "admin/candidat/createCandidat";
        }
        Candidat candidat = candidatService.toCandidat(utilisateurDTO);
        candidatService.save(utilisateurDTO);
        redirectAttributes.addFlashAttribute("successMessage", "Ajout d'un candidat " + candidat.getNom());
        return "redirect:/admin/candidat/candidats";

    }
}
