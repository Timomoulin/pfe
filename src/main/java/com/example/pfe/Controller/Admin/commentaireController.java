
package com.example.pfe.Controller.Admin;

import com.example.pfe.DAO.CommentaireDAO;
import com.example.pfe.Model.Commentaire;
import com.example.pfe.Service.CommentaireService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller

public class commentaireController {
    private CommentaireService commentaireService;
    @Autowired


    public commentaireController(CommentaireService commentaireService) {
        this.commentaireService = commentaireService;
    }





    @GetMapping("/admin/commentaire/commentaires")
    public  String commentaires(Model model){
        model.addAttribute("Commentaires",commentaireService.getAllCommentaires());
        return  "admin/commentaire/commentaires";
    }

    @GetMapping("/admin/commentaire/create")
    public String createCommentaire(Model model){
        Commentaire commentaire=new Commentaire();
        model.addAttribute("commentaire",commentaire);
        return "admin/commentaire/create_Commentaire";}

    @PostMapping("/Commentaires")
    public String saveCommentaire(@PathVariable("commentaire") Commentaire commentaire){
        commentaireService.saveCommentaire(commentaire);
        return "redirect:/commenatires";}

    @GetMapping("/commentaires/edit{id}")
    public String editCommentaire(@PathVariable Long id,Model model) {
        model.addAttribute("commentaire",commentaireService.getCommentaireById(id));
        return "edit_commentaire";
    }

    @PostMapping("/commentaires/{id}")
    public String updateCommentaire(@PathVariable Long id, @ModelAttribute("commentaire")Commentaire commentaire,Model model){
        Commentaire existingCommentaire=commentaireService.getCommentaireById(id);
        existingCommentaire.setId(id);
        existingCommentaire.setNote(commentaire.getNote());
        commentaireService.updateCommentaire(existingCommentaire);
        return "redirect:/commentaires";
}
    @GetMapping("/commentaires/{id}")
    public String deletedCommentaire(@PathVariable Long id){
        commentaireService.deleteCommentaireById(id);
        return "redirect:/commentaires";
    }

}
