/**
package com.example.pfe.Controller.Admin;

import com.example.pfe.DAO.TuteurDAO;
import com.example.pfe.Model.Tuteur;
import com.example.pfe.Service.TuteurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class tuteurController {
    private TuteurService tuteurService;
    private final TuteurDAO tuteurDAO;

    @Autowired

    public tuteurController(TuteurService tuteurService, TuteurDAO tuteurDAO) {
        this.tuteurService = tuteurService;
        this.tuteurDAO = tuteurDAO;
    }


    @GetMapping("/admin/tuteur/tuteurs")
    public  String tuteurs(Model model){
        model.addAttribute("Tuteurs",tuteurService.getAllTuteurs());
        return  "admin/tuteur/tuteurs";
    }

    @GetMapping("/admin/tuteur/create")
    public  String createTuteur(Model model){
        Tuteur tuteur=new Tuteur();
        model.addAttribute("tuteur",tuteur);
        return "redirect:/tuteurs";
    }

    @GetMapping("/tuteurs/edit{id}")
    public String editTuteur(@PathVariable Long id,Model model){
        model.addAttribute("tuteur",tuteurService.getTuteurById(id));
                return "edit_tuteur";
    }

    @PostMapping("/tuteurs/{id}")
    public String updateTuteur(@PathVariable Long id, @ModelAttribute("tuteur")Tuteur tuteur,Model model){
        Tuteur existingTuteur=tuteurService.getTuteurById(id);
        existingTuteur.setNom(tuteur.getNom());
        existingTuteur.setPrenom(tuteur.getPrenom());
        existingTuteur.setSpecialite(tuteur.getSpecialite());
        existingTuteur.setNbrCandidat(tuteur.getNbrCandidat());

        tuteurService.updateTuteur(existingTuteur);
        return "redirect:/tuteurs";

    }

    @GetMapping("/tuteur/{id}")
    public  String deleteTuteur(@PathVariable Long id){
        tuteurService.deleteTuteurById(id);
        return "redirect:/tuteurs";

    }

}
**/
