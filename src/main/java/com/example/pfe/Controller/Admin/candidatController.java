
package com.example.pfe.Controller.Admin;
import com.example.pfe.DTO.UtilisateurDTO;
import com.example.pfe.Model.Candidat;
import com.example.pfe.Service.CandidatService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


@Controller
public class candidatController {

    CandidatService candidatService;

    @Autowired

    public candidatController(CandidatService candidatService) {
        this.candidatService = candidatService;
    }

    @GetMapping("/admin/candidat/candidats")
    public String index(Model model, @RequestParam(name = "page", defaultValue = "0") int page,
                        @RequestParam(name = "size", defaultValue = "4") int size,
                        @RequestParam(name = "keyword", defaultValue = "") String keyword) {
        Page<Candidat> candidatPage = candidatService.candidatPage(keyword, PageRequest.of(page, size));
        model.addAttribute("candidats", candidatPage.getContent());
        model.addAttribute("pages", new int[candidatPage.getTotalPages()]);
        model.addAttribute("currentPage", page);
        model.addAttribute("keyword", keyword);

        return "/admin/candidat/candidats";
    }

    /**
     * Affiche le formulaire d'inscription
     *
     */
    @GetMapping("/inscription")
    public String inscription(Model model){
        model.addAttribute("candidatDTO",new UtilisateurDTO());
        return "visiteur/inscription";
    }

    @PostMapping("/inscription")
    public String traitementInscription(@Valid @ModelAttribute UtilisateurDTO utilisateurDTO, BindingResult bindingResult, RedirectAttributes redirectAttributes){
        if(bindingResult.hasErrors()){
            redirectAttributes.addFlashAttribute("errorMessage", "Le formulaire comporte une erreur");
            return "visiteur/inscription";
        }
        candidatService.inscrireClient(utilisateurDTO);
        redirectAttributes.addFlashAttribute("successMessage", "Inscription réusie");
        return "redirect:/login";

    }



    @GetMapping("/admin/candidat/edit/{id}")
    public String editCandidat(@PathVariable long id, Model model) {
        Candidat candidat =candidatService.getCandidat(id);
        UtilisateurDTO utilisateurDTO =candidatService.toCandidatDTO(candidat);
        model.addAttribute("candidat",candidatService.getAll());
        model.addAttribute("candidatDTO", utilisateurDTO);


        return "/admin/candidat/editCandidat";
    }


    @PostMapping("/admin/candidat/candidats")
    public String uppCandidat(Model model, @ModelAttribute UtilisateurDTO utilisateurDTO, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("errorMessage", "Un problème est survenu lors du traitement du formulaire");
            model.addAttribute("candidat", candidatService.getAll());
            return "/admin/candiat/editCandidat";
        }
        candidatService.save(utilisateurDTO);
        redirectAttributes.addFlashAttribute("successMessage", "Modification d'un candidat " + utilisateurDTO.getNom());
        return "redirect:/admin/candidat/candidats";
    }

    @PostMapping("admin/candidat/candidats/delete")
    public String delete(@RequestParam Long id,RedirectAttributes redirectAttributes ){
        /** @RequestParam(name="keyword" defaultValue="" ) String keyword,
         @RequestParam(name="page" defaultValue="0" )
         int page);**/

        this.candidatService.deleteCandidatById(id);
        redirectAttributes.addFlashAttribute("successMessage","Suppresion du candidat");
        return "redirect:/admin/candidat/cnaidats";
        /**    ?page="+page+"keyword"+keyword;**/

    }
}