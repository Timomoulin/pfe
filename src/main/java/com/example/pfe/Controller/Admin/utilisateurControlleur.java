package com.example.pfe.Controller.Admin;

import com.example.pfe.Service.UtilisateurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;


@Controller

public class utilisateurControlleur {

    @Autowired
    private UtilisateurService utilisateurService;


    public utilisateurControlleur(UtilisateurService utilisateurService) {
        this.utilisateurService = utilisateurService;
    }


    /**
     * Affiche le formulaire de Login TODO
     */
    @GetMapping("/login")
    public String login() {
        return "visiteur/login";
    }


    /**
     * EXEMPLE DE PAGE VISITEUR
     * Dirige vers la page d'acceuil (visible par tout les utilisateurs)
     */
    @GetMapping("/accueil")
    public String accueil() {
        return "visiteur/accueil";
    }

    /**
     * EXEMPLE DE PAGE CLIENT/ADMIN
     * Dirige vers la page le dashboard du client ou admin (visible seulement si l'utilisateur est un client ou admin)
     */
    @GetMapping("/tuteur/truc")
    public String clientDash() {
        return "tuteur/test";
    }


}