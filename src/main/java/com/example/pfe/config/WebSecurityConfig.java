package com.example.pfe.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig {

    @Bean
    public PasswordEncoder getEncoder(){
        return new BCryptPasswordEncoder();
    }

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity httpSecurity) throws Exception {
        return httpSecurity

                //c est pour créer un filtre on utulise la méthode SecurityFilterChain

                .csrf(csrf -> csrf.disable())
                .formLogin(form -> form
                        .loginPage("/login").defaultSuccessUrl("/accueil")
                        .permitAll()
                )
                .logout(logout-> logout
                        .logoutUrl("/logout")
                        .permitAll()
                )
                //dire à spring security toute les reqûette nécessite
                // une authentification.anyRequest().authentificated()
                .authorizeHttpRequests(auth -> auth
                        .requestMatchers("/accueil","/inscription","/css/**", "/js/**", "/img/**", "/favicon.ico").permitAll()
                        .requestMatchers(HttpMethod.POST,("/inscription")).permitAll()
                        //Interdit la page si l'utilisateur n'est pas admin
                        .requestMatchers("/admin/**").hasAuthority("admin")
                        .requestMatchers("/tuteur/**").hasAnyAuthority("admin","tuteur")
                        .requestMatchers("/candidat/**").hasAnyAuthority("candidat","admin","tuteur")
                        .anyRequest().authenticated()
                )
                .build();
    }

}


