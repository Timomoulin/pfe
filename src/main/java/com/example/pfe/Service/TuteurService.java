
package com.example.pfe.Service;

import com.example.pfe.DAO.TuteurDAO;
import com.example.pfe.Model.Tuteur;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class TuteurService {

    private TuteurDAO tuteurDAO;

    @Autowired

    public TuteurService(TuteurDAO tuteurDAO) {
        this.tuteurDAO = tuteurDAO;
    }


    public List<Tuteur>getAllTuteurs(){
        return this.tuteurDAO.findAll();
    }

    public Tuteur saveTuteur(Tuteur tuteur
    ){ return tuteurDAO.save(tuteur);}

    public   Tuteur getTuteurById(Long id){
        return tuteurDAO.findById(id).orElse(null);
    }

    public  Tuteur updateTuteur(Tuteur tuteur){ return tuteurDAO.save(tuteur);}

//    public  List<Tuteur>searchNom(String nom){
//        return tuteurDAO.findTuteurByNomContainingIgnoreCase(nom);
//    }

    public void deleteTuteurById(Long id){tuteurDAO.deleteById(id);}

}
