package com.example.pfe.Service;


import com.example.pfe.DAO.UtilisateurDAO;
import com.example.pfe.DTO.FormUtilisateurAdminDTO;
import com.example.pfe.Model.Candidat;
import com.example.pfe.Model.Utilisateur;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;


@Service
public class UtilisateurService {
 private UtilisateurDAO utilisateurDAO;
 private PasswordEncoder encoder;

@Autowired
 public UtilisateurService(UtilisateurDAO utilisateurDAO, PasswordEncoder encoder) {
  this.utilisateurDAO = utilisateurDAO;
  this.encoder = encoder;
 }



// public Utilisateur inscrireClient(CandidatDTO dto){
//  Utilisateur user=this.convertToEntity(dto);
//  user.setEmail(user.getEmail());
//  user.setMdp(encoder.encode(user.getMdp()));
//  System.out.println(user.getMdp());
//  return utilisateurDAO.save(user);
// }



 public Utilisateur convertToEntity(FormUtilisateurAdminDTO dto){
  Utilisateur utilisateur=null;

  if(dto.getRole()=="admin"){
//   utilisateur = new Admin();
  } else if (dto.getRole() == "tuteur") {
   
  } else if (dto.getRole() == "candidat") {
   utilisateur = new Candidat(dto.getId(),dto.getNom(),dto.getPrenom(),dto.getEmail(),encoder.encode(dto.getMdp1()));
  }
  utilisateurDAO.save(utilisateur);
  return utilisateur;
 }
 }



