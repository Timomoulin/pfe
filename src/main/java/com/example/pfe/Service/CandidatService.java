
package com.example.pfe.Service;

import com.example.pfe.DAO.CandidatDAO;
import com.example.pfe.DTO.UtilisateurDTO;
import com.example.pfe.Model.Candidat;
import com.example.pfe.Model.Utilisateur;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class CandidatService {

   private CandidatDAO candidatDAO;
    private PasswordEncoder encoder;


    @Autowired

    public CandidatService(CandidatDAO candidatDAO, PasswordEncoder bcryptEncoder) {
        this.encoder =bcryptEncoder;
        this.candidatDAO = candidatDAO;
    }

    public List<Candidat> getAll(){ return candidatDAO.findAll();}

    public Candidat getCandidat(Long id){
        Candidat candidat=candidatDAO.findById(id).orElseThrow();
        return candidat;
    }

    public   Candidat getCandidatById(Long id){
        return candidatDAO.findById(id).orElse(null);
    }

    public Page<Candidat>candidatPage(String key, Pageable page){

        return this.candidatDAO.findByNomContainingIgnoreCase(key,page);
    }


    public  Candidat save(UtilisateurDTO utilisateurDTO){
      Candidat candidat=this.toCandidat(utilisateurDTO);


      return candidatDAO.save(candidat);
  }
    public  Candidat saveCandidat(Candidat candidat){ return candidatDAO.save(candidat);
    }

    public Utilisateur inscrireClient(UtilisateurDTO dto){
        Candidat user=this.toCandidat(dto);
        user.setEmail(user.getEmail());
        user.setMdp(encoder.encode(user.getMdp()));
        System.out.println(user.getMdp());
        return candidatDAO.save(user);
    }

    public Candidat toCandidat(UtilisateurDTO utilisateurDTO){
        Candidat candidat;
        if (utilisateurDTO.getId()!=null){
           candidat = candidatDAO.findById(utilisateurDTO.getId()).orElseThrow();
        }
        else {
            candidat = new Candidat();
        }
        candidat.setNom(utilisateurDTO.getNom());
        candidat.setPrenom(utilisateurDTO.getPrenom());
        candidat.setEmail(utilisateurDTO.getEmail());
        candidat.setMdp(utilisateurDTO.getMdp1());
        candidat.setRentreeList(utilisateurDTO.getRentreeList());
        candidat.setEquipeList(utilisateurDTO.getEquipeList());
        return candidat;
    }

    public UtilisateurDTO toCandidatDTO(Candidat candidat){
        UtilisateurDTO utilisateurDTO = new UtilisateurDTO();

        utilisateurDTO.setId(candidat.getId());
        utilisateurDTO.setNom(candidat.getNom());
        utilisateurDTO.setPrenom(candidat.getPrenom());
        utilisateurDTO.setEmail(candidat.getEmail());
//        candidatDTO.setMdp1(candidat.getMdp());
        utilisateurDTO.setRentreeList(candidat.getRentreeList());
        utilisateurDTO.setEquipeList(candidat.getEquipeList());

        return utilisateurDTO;
    }





    public void deleteCandidatById(Long id){candidatDAO.deleteById(id);}
}
