
package com.example.pfe.Service;

import com.example.pfe.DAO.CommentaireDAO;
import com.example.pfe.Model.Commentaire;
import com.example.pfe.Model.Projet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class CommentaireService {

    private  CommentaireDAO commentaireDAO;

    @Autowired

    public CommentaireService(CommentaireDAO commentaireDAO) {
        this.commentaireDAO = commentaireDAO;
    }

    public List<Commentaire> getAllCommentaires() {
        return this.commentaireDAO.findAll();
    }


    public Commentaire saveCommentaire(Commentaire commentaire) {
        return commentaireDAO.save(commentaire);
    }

    public Commentaire getCommentaireById(Long id) {
        return commentaireDAO.findById(id).orElse(null);
    }

    public Commentaire updateCommentaire(Commentaire commentaire) {
        return commentaireDAO.save(commentaire);
    }

    public Page<Commentaire> pageCommentaires(String key,Pageable page) {
        return this.commentaireDAO.findByNoteContainingIgnoreCase(key,page);
    }


    public void deleteCommentaireById(Long id) {
        commentaireDAO.deleteById(id);
    }
}