
package com.example.pfe.Service;

import com.example.pfe.DAO.RentreeDAO;
import com.example.pfe.DTO.ProjetDTO;
import com.example.pfe.DTO.RentreeDTO;

import com.example.pfe.Model.Equipe;
import com.example.pfe.Model.Projet;
import com.example.pfe.Model.Rentree;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class RentreeService {

    private RentreeDAO rentreeDAO;



    public RentreeService(RentreeDAO rentreeDAO) {
        this.rentreeDAO = rentreeDAO;
    }


    public List<Rentree> getAll(){return rentreeDAO.findAll();}



  /**public Page<Rentree>rentreesPage(String keyword, PageRequest of){

      return (Page<Rentree>) this.rentreeDAO.findAll();**/


    public Page<Rentree>rentreePage(String key, Pageable page){
          return this.rentreeDAO.findByBrancheContainingIgnoreCase(key, page);}


    public Rentree getRentree(Long id){
        Rentree rentree= rentreeDAO.findById(id).orElseThrow();
        return rentree;
    }

   /** public List<Rentree>getAll(){List<Rentree> rentrees=rentreeDAO.findAll();
       return rentrees;
   }
**/

/**public Page<Rentree>rentreePage(String key,Pageable page){
    return  this.rentreeDAO.findAll(key,page);}
**/

  /**  public  Rentree save(Rentree rentree){
        return rentreeDAO.save(rentree);
    }**/


   /**public Rentree save(){
        Rentree rentree=this.toRentree(rentreeDTO);


        return rentreeDAO.save(rentree);
    }**/
   public  Rentree save(RentreeDTO rentreeDTO){
       Rentree rentree=this.toRentree(rentreeDTO);


       return rentreeDAO.save(rentree);
   }


    public Rentree getRentreeById(Long id){
        return rentreeDAO.findById(id).orElse(null);
    }

    public Rentree updateRentree(Rentree rentree){ return rentreeDAO
            .save(rentree);}

   /** public  List<Session>searchBranche(String branche){
        return sessionDAO.findSessionByBrancheContainingIgnoreCase(branche);
    }**/

   /**public Page<Rentree> rentreePage(String key, Pageable page){
       return (Page<Rentree>) this.rentreeDAO.findByBrancheIgnoreCase(key, page);

   }
   /**
    public Page<Rentree>rentreePage(String key, Pageable page){
       return (Page<Rentree>) this.rentreeDAO.getTotalPages(key,page);}**/


    public Rentree toRentree(RentreeDTO rentreeDTO){
        Rentree rentree;


        if(rentreeDTO.getId()!=null){
            rentree = rentreeDAO.findById(rentreeDTO.getId()).orElseThrow();
        }


        else{
            rentree = new Rentree();
        }


        rentree.setAnnee(rentreeDTO.getAnnee());
        rentree.setBranche(rentreeDTO.getBranche());

        return rentree;
    }


    public RentreeDTO toRentreeDTO(Rentree rentree){
        RentreeDTO rentreeDTO =new RentreeDTO();

        rentreeDTO.setId(rentree.getId());
        rentreeDTO.setBranche(rentree.getBranche());

        rentreeDTO.setProjetList((rentree.getProjetList()));
        rentreeDTO.setAnnee(rentree.getAnnee());

        return rentreeDTO;

    }


    public void deleteRentreeById(Long id){
        rentreeDAO.deleteById(id);}



}

