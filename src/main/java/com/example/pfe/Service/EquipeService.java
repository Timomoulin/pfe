package com.example.pfe.Service;


import com.example.pfe.DAO.EquipeDAO;
import com.example.pfe.DTO.EquipeDTO;
import com.example.pfe.Model.Equipe;
import com.example.pfe.Model.Rentree;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EquipeService {

    private EquipeDAO equipeDAO;

    @Autowired

    public EquipeService(EquipeDAO equipeDAO) {
        this.equipeDAO = equipeDAO;
    }

    public List<Equipe>getAll(){
        return  equipeDAO.findAll();
    }

    public Equipe getEquipeById(Long id){
        return equipeDAO.findById(id).orElse(null);
    }

    public Page<Equipe>pageEquipes(String key, Pageable page){
        return this.equipeDAO.findByNomContainsIgnoreCase(key, page);}

   public  Equipe save(EquipeDTO equipeDTO){
        Equipe equipe=this.toEquipe(equipeDTO);
        return equipeDAO.save(equipe);
    }
   /** public Equipe save(Equipe equipe){
        return equipeDAO.save(equipe);
    }**/

    public Equipe toEquipe(EquipeDTO equipeDTO) {
        Equipe equipe;
        if (equipeDTO.getId() != null) {
            equipe = equipeDAO.findById(equipeDTO.getId()).orElseThrow();

        } else {
            equipe = new Equipe();
        }
        equipe.setNom(equipeDTO.getNom());
        return equipe;
    }
    public EquipeDTO toEquipeDTO(Equipe equipe){
        EquipeDTO equipeDTO=new EquipeDTO();

        equipeDTO.setId(equipe.getId());
        equipeDTO.setNom(equipe.getNom());
        equipeDTO.setProjet(equipe.getProjet());

        return equipeDTO;
    }

    public void deleteEquipeById(Long id){
        equipeDAO.deleteById(id);}


}
