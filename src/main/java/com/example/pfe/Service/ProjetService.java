package com.example.pfe.Service;

import com.example.pfe.DAO.ProjetDAO;
import com.example.pfe.DTO.ProjetDTO;
import com.example.pfe.Model.Projet;

import com.example.pfe.Model.Rentree;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.sql.Date;
import java.util.List;

@Service
public class ProjetService  {
    /**
     * car j'ai besoin d'acceder à la base de donnée donc je dois faire appel au répository ou DAO et @ Authowired
     * pour l'injection des dépondance
     */
    private ProjetDAO projetDAO;

    @Autowired
    public ProjetService(ProjetDAO projetDAO) {
        this.projetDAO = projetDAO;
    }


    public List<Projet> getAll(){return projetDAO.findAll();}


    public Projet getProjet(Long id){
        Projet projet= projetDAO.findById(id).orElseThrow();
        return projet;
    }

    /**
     * je déclare la liste des patients
     * récupére une liste de projet
     *
     */
    public List<Projet>projetList(){
        return projetDAO.findAll();
    }

    /**
     * methode pour sauvgarder le projet elle prend en paramétre comme elle reçoit un ProjetDTO
     * donc on aura besoin de convertir notre projetDTO en Projet(entity)
     * @param projetDTO
     * @return
     */

    public  Projet save(ProjetDTO projetDTO){
        Projet projet=this.toProjet(projetDTO);


        return projetDAO.save(projet);
    }



   /** public Projet save(Projet projet){
        return projetDAO.save(projet);**/

    /**
     * par le biais du formulaire en reçoit un DTO(projetDTO et on le convertie en une entité projet
     * pour le sauvegarder dans la base de donnée
     * @param
     * @return
     */
    /**
     * la méthode toProjet va recevoir un ProjetDTO
     *il y'a une différence entre le cas d'ajout et le cas de la modification c est que dans l'ajout le projetDTO
     * n'a pas d' id au contraire que la modification qu'on connais son id
     */
    public Projet toProjet(ProjetDTO projetDTO){
        Projet projet;
        /***
         * si jamais mon projet a un id qui n 'est pas null (donc nous sommes dans le cas d'une modification)
         * donc on peux déjà récuperer les infos du projet avant le remplissage du formulaire ou la modification
         * à l'aide de la methode findById si non tu crér un nouveau projet avec l'exception orElse
         *
         * j'ai un id mais si il est null je creer une nouveau projet
         */

        if(projetDTO.getId()!=null){
            projet = projetDAO.findById(projetDTO.getId()).orElseThrow();
        }

        /***
         * dans le cas ou j'ai pas d'id faut creer une nouveau projet
         */
        else{
            projet = new Projet();
        }
/***
 * on fait pas sa sur l'id car si 'ellle a déjà un id on l'a récupérer à la ligne 68
 * donc je récupérer les information par mon dto et les assignées à mon projet
 * donc cette méthode elle fait que retourner un projet elle sauvgarde pas encore
 */

        projet.setNom(projetDTO.getNom());
        projet.setDateDebut(projetDTO.getDateDebut());
        projet.setDateFin(projetDTO.getDateFin());
        return projet;
    }


    /**
     * pour la validation on convertie l'entité en DTO et l'envoyer au client
     * donc j ai besoin qu'on me donne l'id pour récupérer les information et
     * la convertir en Entity et l'envoyer au template
     */

        public ProjetDTO toDTO(Projet projet){
            ProjetDTO projetDTO =new ProjetDTO();


        projetDTO.setId(projet.getId());
        projetDTO.setNom(projet.getNom());
        projetDTO.setDateDebut(projet.getDateDebut());
        projetDTO.setDateFin(projet.getDateFin());
            /***
             * faut recuperer la session par le projet  et la convertir en toDTO
             */
        projetDTO.setRentree((projet.getRentree()));
       projetDTO.setEquipeList((projet.getEquipeList()));
        return projetDTO;
    }









    public   Projet findById(Long id){
        return projetDAO.findById(id).orElse(null);
    }

    /**
     * c 'à dire trouve le project la méthode findProject() on utulise @PathVariable pour dirre cherche moi le id qui se trouve dans
     * le param.
     * on utulisant la ùéthode get() car nous somme certains que le produit existe si non on utulise orElse(null)
     * @param 'id'*/
     /**** @return
**/
    public  Projet findProject(@PathVariable Long id){
        return projetDAO.findById(id).get();
    }

    public  Projet updateProjet(Projet projet){ return projetDAO.save(projet);}

   /**public  Page<Projet> pageProjets(String keyword, Pageable pageable){
        return projetDAO.findByNomContainingIgnoreCase(keyword,pageable);
    }**/

    /**pour afficher les projets on utulise la méthode findAll() ui va retourner une List de projet et la parcourir
     *
     * public  Page<Projet>pageProjets= projetDAO.findByNomIgnoreCase();
    }**/
     public Page<Projet>pageProjets(String key, Pageable page){
         return this.projetDAO.findByNomContainingIgnoreCase(key,page);

    }
    public void deleteProjetById(Long id){projetDAO.deleteById(id);}
}
