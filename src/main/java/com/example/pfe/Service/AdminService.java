package com.example.pfe.Service;

import com.example.pfe.DAO.AdminDAO;
import com.example.pfe.Model.Admin;
import com.example.pfe.Model.Candidat;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class AdminService {

    private AdminDAO adminDAO;

    @Autowired

    public AdminService(AdminDAO adminDAO) {
        this.adminDAO = adminDAO;
    }

    public List<Admin>getAll(){
        return adminDAO.findAll();
    }


    public Admin getAdmin(Long id){
        Admin admin =adminDAO.findById(id).orElseThrow();
        return admin;
    }
}
