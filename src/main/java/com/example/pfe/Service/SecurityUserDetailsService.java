package com.example.pfe.Service;

import com.example.pfe.DAO.UtilisateurDAO;
import com.example.pfe.Model.Utilisateur;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;
@Service
public class SecurityUserDetailsService implements UserDetailsService {
    private UtilisateurDAO utilisateurDAO;
    private PasswordEncoder encoder;

    @Autowired

    public SecurityUserDetailsService(UtilisateurDAO utilisateurDAO, PasswordEncoder encoder) {
        this.utilisateurDAO = utilisateurDAO;
        this.encoder = encoder;
    }
    /***
     * Une méthode qui prend en param un nom d'utilisateur ou email qui provient du formulaire de login.
     * Et retourne un objet User (class provenant de Security)  avec un username,password (encoder), une liste de permisions
     * @param username peudo ou email du form de login
     * @return  un objet User (qui implement l'interface UserDetails)
     * @throws UsernameNotFoundException
     **/
  @Override
    @Transactional(readOnly = true)
    public  UserDetails loadUserByUsername(String username) throws UsernameNotFoundException{
        Utilisateur utilisateur=utilisateurDAO.findByEmail(username);
        if (utilisateur==null){ throw new UsernameNotFoundException("Utilisateur non trouvé"+username);
        }

        String nomRole=utilisateur.getClass().getSimpleName();
        //Etape 4 je consitu un lise de permision
        Set<GrantedAuthority> permisions= new HashSet<>();
        //Etape 5 J'ajoute le nom du role dans la liste de permision.
        permisions.add(new SimpleGrantedAuthority(nomRole));
        //Etape 6 je créer un objet User
        User user= new User(utilisateur.getEmail(),utilisateur.getMdp(),permisions);
        return user;
}
}

//spring extra tymeleaf security indepondance pour afficher le nom d'utulisateur
  /** je vais utuliser un th:text=avec une expression(#) ${#authentification ou authaurisation }donc je demande à soring securitu de
   m'afficher le nom de l'utulisateur qu est authentifié
   th:text="${#authentication.name}"
**/