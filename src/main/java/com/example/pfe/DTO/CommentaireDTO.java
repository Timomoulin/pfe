
package com.example.pfe.DTO;

public class CommentaireDTO {

    private Long id;
    private String note;

    public CommentaireDTO() {
    }

    public CommentaireDTO(Long id, String note) {
        this.id = id;
        this.note = note;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}