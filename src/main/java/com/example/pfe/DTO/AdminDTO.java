package com.example.pfe.DTO;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;

public class AdminDTO {

    private Long id;
    @NotBlank(message = "il faut un nom")
    @Size(min = 3,message = "le nom est trop court")
    private String nom;

    @NotBlank(message = "Prenom est requis")
    @Size(min =3,message = "le prenom est trop court")
    private String prenom;


    @NotBlank(message = "Email est requis")
    @Email( message = "Email n'est pas valide")
    private String email;
    private String mdp;

    public AdminDTO() {
    }

    public AdminDTO(Long id, String nom, String prenom, String email, String mdp) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.mdp = mdp;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMdp() {
        return mdp;
    }

    public void setMdp(String mdp) {
        this.mdp = mdp;
    }
}
