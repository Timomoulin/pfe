
package com.example.pfe.DTO;

import com.example.pfe.Model.Candidat;
import com.example.pfe.Model.Projet;
;
import java.sql.Date;
import java.util.List;


public class RentreeDTO {

    private Long id;
    private String branche;
    private Date annee;

    List<Candidat>candidatList;

    public List<Candidat> getCandidatList() {
        return candidatList;
    }

    public void setCandidatList(List<Candidat> candidatList) {
        this.candidatList = candidatList;
    }

    List<Projet>projetList;

    public List<Projet> getProjetList() {
        return projetList;
    }

    public void setProjetList(List<Projet> projetList) {
        this.projetList = projetList;
    }

    public RentreeDTO(Long id, String branche, Date annee) {
        this.id = id;
        this.branche = branche;
        this.annee = annee;
    }

    public RentreeDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBranche() {
        return branche;
    }

    public void setBranche(String branche) {
        this.branche = branche;
    }

    public Date getAnnee() {
        return annee;
    }

    public void setAnnee(Date annee) {
        this.annee = annee;
    }
}

