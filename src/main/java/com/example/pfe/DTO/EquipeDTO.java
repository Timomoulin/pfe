package com.example.pfe.DTO;

import com.example.pfe.Model.Candidat;
import com.example.pfe.Model.Projet;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;

import java.util.List;

public class EquipeDTO  {

    private Long id;
    @NotBlank(message = "il faut un nom")
    @Size(min = 3,message = "le nom est trop court")
    private String nom;

    List<Candidat>candidatList;

    public List<Candidat> getCandidatList() {
        return candidatList;
    }

    public void setCandidatList(List<Candidat> candidatList) {
        this.candidatList = candidatList;
    }

    private Projet projet;

    public Projet getProjet() {
        return projet;
    }

    public void setProjet(Projet projet) {
        this.projet = projet;
    }

    public EquipeDTO() {
    }

    public EquipeDTO(Long id, String nom) {
        this.id = id;
        this.nom = nom;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }


}
