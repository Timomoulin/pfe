package com.example.pfe.DTO;

public class FormUtilisateurAdminDTO extends UtilisateurDTO{

    public FormUtilisateurAdminDTO(Long id, String nom, String prenom, String email, String mdp, String role) {
        super(id, nom, prenom, email, mdp);
        this.role = role;
    }

    public FormUtilisateurAdminDTO() {

    }

    private String role;

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
