
package com.example.pfe.DTO;

import com.example.pfe.Annotation.PasswordCorres;
import com.example.pfe.Model.Equipe;
import com.example.pfe.Model.Rentree;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;

import java.util.List;


@PasswordCorres(message = "les mots de passe ne sont pas identiques")
public class UtilisateurDTO {

    private Long id;
    @NotBlank(message = "il faut un nom")
    @Size(min = 3,message = "le nom est trop court")
    private String nom;

    @NotBlank(message = "Prenom est requis")
    @Size(min =3,message = "le prenom est trop court")
    private String prenom;

    @NotBlank(message = "Le code est resuis")
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @NotBlank(message = "Email est requis")
    @Email( message = "Email n'est pas valide")
    private String email;


    @NotEmpty(message = "Le champ mdp1 ne peut pas être vide")
    private String mdp1;

    @NotEmpty(message = "Le champ mdp2 ne peut pas être vide")
    private String mdp2;
    public UtilisateurDTO(Long id, String nom, String prenom, String email, String mdp) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;

    }
    List<Rentree>rentreeList;

    public List<Rentree> getRentreeList() {
        return rentreeList;
    }

    public void setRentreeList(List<Rentree> rentreeList) {
        this.rentreeList = rentreeList;
    }

    List<Equipe>equipeList;

    public List<Equipe> getEquipeList() {
        return equipeList;
    }

    public void setEquipeList(List<Equipe> equipeList) {
        this.equipeList = equipeList;
    }

    public UtilisateurDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }




    public String getMdp1() {
        return mdp1;
    }

    public void setMdp1(String mdp1) {
        this.mdp1 = mdp1;
    }

    public String getMdp2() {
        return mdp2;
    }

    public void setMdp2(String mdp2) {
        this.mdp2 = mdp2;
    }

}