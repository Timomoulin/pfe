
package com.example.pfe.Model;

import jakarta.persistence.*;


import java.sql.Date;

import java.util.LinkedList;
import java.util.List;

@Entity
public class Rentree {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;
    private String branche;

    private String code;
    private Date annee;

    @ManyToOne
    @JoinColumn(name = "idAdmin")
    private Admin admin;

    @ManyToMany
    @JoinTable(name = "candidat_rentree",joinColumns = @JoinColumn(name = "rentree_id"),
            inverseJoinColumns = @JoinColumn(name ="candidat_id" ))
    private List<Candidat>candidatList=new LinkedList<>();

    @OneToMany(mappedBy = "rentree", cascade = CascadeType.ALL)
    private List<Projet>ProjetList;

    public Rentree(Long id, String branche, Date annee) {
        this.id = id;
        this.branche = branche;
        this.annee = annee;
    }


    public Rentree() {
    }

    public Long getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Rentree rentree = (Rentree) o;

        if (!id.equals(rentree.id)) return false;
        if (!branche.equals(rentree.branche)) return false;
        return annee.equals(rentree.annee);
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + branche.hashCode();
        result = 31 * result + annee.hashCode();
        return result;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBranche() {
        return branche;
    }

    public void setBranche(String branche) {
        this.branche = branche;
    }

    public Date getAnnee() {
        return annee;
    }

    public void setAnnee(Date annee) {
        this.annee = annee;
    }

    public List<Projet> getProjetList() {
        return ProjetList;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setProjetList(List<Projet> projetList) {
        ProjetList = projetList;
    }

    public List<Candidat> getCandidatList() {
        return candidatList;
    }

    public void setCandidatList(List<Candidat> candidatList) {
        this.candidatList = candidatList;
    }

}
