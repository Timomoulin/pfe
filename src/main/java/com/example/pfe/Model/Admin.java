package com.example.pfe.Model;

import jakarta.persistence.*;

import java.util.List;

@Entity
public class Admin {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;
    @Column(name = "nom", nullable = false)
    private String nom;

    @Column
    private String prenom;
    private String email;
    private String mdp;


    @OneToMany(mappedBy = "admin",cascade = CascadeType.ALL)
    private List<Rentree> rentreeList;

    public Admin() {
    }

    public Admin(Long id, String nom, String prenom, String email, String mdp) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.mdp = mdp;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMdp() {
        return mdp;
    }

    public void setMdp(String mdp) {
        this.mdp = mdp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Admin admin = (Admin) o;

        if (!id.equals(admin.id)) return false;
        if (!nom.equals(admin.nom)) return false;
        if (!prenom.equals(admin.prenom)) return false;
        if (!email.equals(admin.email)) return false;
        return mdp.equals(admin.mdp);
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + nom.hashCode();
        result = 31 * result + prenom.hashCode();
        result = 31 * result + email.hashCode();
        result = 31 * result + mdp.hashCode();
        return result;
    }
}
