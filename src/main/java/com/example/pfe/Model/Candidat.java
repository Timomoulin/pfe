package com.example.pfe.Model;

import com.example.pfe.DAO.EquipeDAO;
import jakarta.persistence.*;
import org.hibernate.mapping.Set;
import org.springframework.data.domain.Page;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
@Entity
@DiscriminatorValue(value="candidat")
public class Candidat extends Utilisateur {




    @ManyToMany(mappedBy = "candidatList", cascade = CascadeType.ALL)
    private List<Rentree> rentreeList = new LinkedList<>();


    @ManyToMany(mappedBy = "candidatList", cascade = CascadeType.ALL)
    private List<Equipe> equipeList = new LinkedList<>();

    public Candidat() {
    }

    public Candidat(Long id, String nom, String prenom, String email, String mdp){
        super(id,prenom,nom,email,mdp);

    }




    public List<Rentree> getRentreeList() {
        return rentreeList;
    }

    public void setRentreeList(List<Rentree> rentreeList) {
        this.rentreeList = rentreeList;
    }

    public List<Equipe> getEquipeList() {
        return equipeList;
    }

    public void setEquipeList(List<Equipe> equipeList) {
        this.equipeList = equipeList;
    }


}

