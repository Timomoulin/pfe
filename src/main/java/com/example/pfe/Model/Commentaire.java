
package com.example.pfe.Model;

import jakarta.persistence.*;

import java.util.List;
import java.util.Objects;

@Entity
public class Commentaire {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    private String note;

    public Commentaire() {
    }

    public Commentaire(Long id, String note) {
        this.id = id;
        this.note = note;
    }

    @ManyToOne()
    @JoinColumn(name = "idTuteur")
    private Tuteur tuteur;

    @ManyToOne()
    @JoinColumn(name = "idEquipe")
    private Equipe equipe;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Commentaire that = (Commentaire) o;

        return Objects.equals(note, that.note);
    }

    @Override
    public int hashCode() {
        return note != null ? note.hashCode() : 0;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public com.example.pfe.Model.Tuteur getTuteur() {
        return tuteur;
    }

    public void setTuteur(com.example.pfe.Model.Tuteur tuteur) {
        this.tuteur = tuteur;
    }

    public com.example.pfe.Model.Equipe getEquipe() {
        return equipe;
    }

    public void setEquipe(com.example.pfe.Model.Equipe equipe) {
        this.equipe = equipe;
    }
}


