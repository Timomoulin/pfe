
package com.example.pfe.Model;

//import com.example.pfe.Annotation.PasswordCorres;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotEmpty;
//@PasswordCorres
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type_utilisateur")
@Table(name = "utilisateur")
public abstract class Utilisateur {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "prenom")
    private String prenom;

    @Column(name = "nom")
    private String nom;

    @Column(name = "email")
    private String email;

    @Column(name = "mdp")
    private String mdp;

    public Utilisateur() {
    }

    public Utilisateur(Long id, String prenom, String nom, String email, String mdp) {
        this.id = id;
        this.prenom = prenom;
        this.nom = nom;
        this.email = email;
        this.mdp = mdp;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMdp() {
        return mdp;
    }

    public void setMdp(String mdp) {
        this.mdp = mdp;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Utilisateur that = (Utilisateur) o;

        if (id != that.id) return false;
        if (!prenom.equals(that.prenom)) return false;
        if (!nom.equals(that.nom)) return false;
        if (!email.equals(that.email)) return false;
        return mdp.equals(that.mdp);
    }

    @Override
    public int hashCode() {
        int result = Math.toIntExact(id);
        result = 31 * result + prenom.hashCode();
        result = 31 * result + nom.hashCode();
        result = 31 * result + email.hashCode();
        result = 31 * result + mdp.hashCode();
        return result;
    }
}