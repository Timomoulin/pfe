
package com.example.pfe.Model;

import jakarta.persistence.*;

import java.util.Objects;

@Entity
public class Tuteur {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;



    private String nom;

    private String prenom;

    private String specialite;
    private Integer nbrCandidat;

    public Tuteur() {
    }

    public Tuteur(Long id, String nom, String prenom, String specialite, Integer nbrCandidat) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.specialite = specialite;
        this.nbrCandidat = nbrCandidat;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }



    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }
    public String getSpecialite() {
        return specialite;
    }

    public void setSpecialite(String specialite) {
        this.specialite = specialite;
    }

    public Integer getNbrCandidat() {
        return nbrCandidat;
    }

    public void setNbrCandidat(Integer nbrCandidat) {
        this.nbrCandidat = nbrCandidat;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Tuteur tuteur = (Tuteur) o;

        if (!id.equals(tuteur.id)) return false;
        if (!nom.equals(tuteur.nom)) return false;
        if (!prenom.equals(tuteur.prenom)) return false;
        if (!specialite.equals(tuteur.specialite)) return false;
        return nbrCandidat.equals(tuteur.nbrCandidat);
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + nom.hashCode();
        result = 31 * result + prenom.hashCode();
        result = 31 * result + specialite.hashCode();
        result = 31 * result + nbrCandidat.hashCode();
        return result;
    }
}
